const Credit = require('../src/index').CreditCardApi;
const StripeConstant = require('../src/index').appConstant;

let stripe = require('stripe')("sk_test_1wqZ49XNYSSYrm42A8c0LDhF00dvUQUF3Q")
// const stripe = require("stripe")("sk_test_4eC39HqLyjWDarjtT1zdp7dc");

stripe.setApiVersion('2018-02-28');

const Credit = new Credit(stripe);
const createToken = async () => {
    try {
        const result = await Credit.creditMethod({
            number: 4242424242424242,
            exp_month: 12,
            exp_year: 2020,
            cvc: '123'
        }, StripeConstant.PAYMENT_METHOD_CREDIT.CREATE_TOKEN)
        console.log(result);
        return result
    } catch (error) {
        console.log('Error:', error.message);
    }
}

const createCustomer = async () => {
    try {
        const customer = await Credit.creditMethod({
            description: 'the first description',
            source: 'tok_1F4mkNGWNqbIGH6soqExqU4d', // obtained with Stripe.js
            email: '12abc@gmail.com'
        }, StripeConstant.PAYMENT_METHOD_CREDIT.CREATE_CUSTOMER)
        console.log(customer);
        return customer
    } catch (error) {
        console.log('Error:', error.message);
    }

}
// createCustomer()

const createCharge = async () => {
    try {
        const charge = await Credit.creditMethod({
            amount: 100,
            currency: 'usd',
            source: 'tok_1F53gBGWNqbIGH6scq3sKik6', // obtained with Stripe.js
            description: "Example charge for payment stripe"
        }, StripeConstant.PAYMENT_METHOD_CREDIT.CREATE_CHARGE)
        console.log(charge);
        return charge
    } catch (error) {
        console.log('Error:', error.message);
    }
}
// createCharge()

const createProduct = async () => {
    try {
        const product = await Credit.creditMethod({
            name: 'T-shirt',
            type: 'good',
            description: 'Comfortable cotton t-shirt',
            attributes: ['size', 'gender']
        }, StripeConstant.PAYMENT_METHOD_CREDIT.CREATE_PRODUCT)
        console.log(product);
        return product
    } catch (error) {
        console.log('Error:', error.message);
    }
}
// createProduct()

const createPlans = async () => {
    try {
        const plan = await Credit.creditMethod({
            amount: 5000,
            interval: "month",
            product: {
                name: 'T-shirt',
                type: 'service',
            },
            currency: "usd",
        }, StripeConstant.PAYMENT_METHOD_CREDIT.CREATE_PLAN)
        console.log(plan);
        return plan
    } catch (error) {
        console.log('Error:', error.message);
    }
}
// createPlans()

const paymentMethods = async () => {
    try {
        const paymentCard = await Credit.creditMethod({
            type: 'card',
            card: {
                number: 4242424242424242,
                exp_month: 12,
                exp_year: 2020,
                cvc: '123'
            }
        }, StripeConstant.PAYMENT_METHOD_CREDIT.PAYMENT_METHOD_CREDIT)
        console.log(paymentCard);
        return paymentCard
    } catch (error) {
        console.log('Error:', error.message);
    }
}
paymentMethods()