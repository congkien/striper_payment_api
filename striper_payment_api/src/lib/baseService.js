const _ = require('lodash');
const axios = require('axios');
const FormData = require('form-data');
const errorMessage = require('./errorMessage');

const postData = async (url, header, data) => {
    try {
        const result = await axios({
            method: 'POST', // you can set what request you want to be
            url,
            data,
            headers: _.merge({
                'Content-Type': `multipart/form-data; boundary=${data._boundary}`,
            }, header)
        })
        return result;
    } catch (error) {
        throw error
    }
}

const getData = async (url, header, params = null) => {
    const result = await axios({
        method: 'GET', // you can set what request you want to be
        url,
        params,
        headers: _.merge({
            'Content-Type': 'application/json',
        }, header)
    })
    return result;
}

const convertToFormData = async (data) => {
    const formData = new FormData();
    _.forEach(data, (value, key) => {
        formData.append([`${key}`], value)
    })
    return formData;
}

const requiredParameters = async (params, agrRequired) => {
    _.forEach(agrRequired, (value) => {
        if (!params[value]) {
            throw Error(`必須パラメーター ${value}`);
        }
    })
}

module.exports = {
    postData,
    getData,
    convertToFormData,
    requiredParameters
}
