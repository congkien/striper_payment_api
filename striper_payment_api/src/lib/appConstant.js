'use strict';

module.exports = Object.freeze({
   DEPOSIT: {
      ACCOUNT: {
         METHOD: {
            CREATE: 1,
            UPDATE: 2,
            DELETE: 3
         }
      },
      ACCOUNT_TYPE: {
         NORMAL: 1,
         CURRENT: 2,
         SAVINGS: 4
      },
      METHOD: {
         CREATE: 1,
         CANCEL: 2
      },
      URL: {
         REGISTRATION_ACCOUNT: 'api/AccountRegistration.idPass',
         SEARCH_ACCOUNT: 'api/AccountSearch.idPass',
         REGISTRATION_DEPOSIT: 'api/DepositRegistration.idPass',
         SEARCH_DEPOSIT: 'api/DepositSearch.idPass',
         REGISTRATION_MAIL_DEPOSIT: 'api/MailDepositRegistration.idPass',
         SEARCH_MAIL_DEPOSIT: 'api/MailDepositSearch.idPass',
         SEARCH_BALANCE: 'api/BalanceSearch.idPass'
      },
      REMITTANCE_METHOD: {
         CREATE_ACCOUNT: 'CreateAccount',
         UPDATE_ACCOUNT: 'UpdateAccount',
         DELETE_ACCOUNT: 'DeleteAccount',
         SEARCH_ACCOUNT: 'SearchAccount',
         CREATE_DEPOSIT: 'CreateDeposit',
         CANCEL_DEPOSIT: 'CancelDeposit',
         SEARCH_DEPOSIT: 'SearchDeposit',
         CREATE_MAIL_DEPOSIT: 'CreateMailDeposit',
         CANCEL_MAIL_DEPOSIT: 'CancelMailDeposit',
         SEARCH_MAIL_DEPOSIT: 'SearchMailDeposit',
         SEARCH_BALANCE: 'SearchBalance'
      }
   },
   PAYMENT_TYPE: {
      CREDIT_CARD: 'Credit',
   },
   PAYMENT_METHOD_CREDIT: {
      CREATE_TOKEN: 'createToken',
      CREATE_CUSTOMER: 'createCustomer',
      CREATE_CHARGE: 'createCharge',
      CREATE_PRODUCT: 'createProduct',
      CREATE_PLAN: 'createPlans'
   },
   MEMBER_METHOD: {
      SAVE_MEMBER: 'SaveMember',
      UPDATE_MEMBER: 'UpdateMember',
      DELETE_MEMBER: 'DeleteMember',
      SEARCH_MEMBER: 'SearchMember',
      TRADE_CARD: 'TradeCard',
      SAVE_CARD: 'SaveCard',
      SEARCH_CARD: 'SearchCard',
      SEARCH_CARD_DETAIL: 'SearchCardDetail',
      DELETE_CARD: 'DeleteCard',
      REGISTER_ACCOUNT: 'RegisterAccount',
      SEARCH_ACCOUNT: 'SearchAccount',
      EXEC_TRAN: 'ExecTran'
   },
   PAYMENT_FORM: {
      ONE_LUMP: 1,
      INSTALLMENT: 2,
      BONUS_ONE_LUMP: 3,
      BONUS_INSTALLMENT: 4,
      REVOLVING: 5
   },
   JOB_CD: {
         UNPROCESSED: 'UNPROCESSED',
         AUTHENTICATED: 'AUTHENTICATED',
         CHECK: 'CHECK',
         CAPTURE: 'CAPTURE',
         SALES: 'SALES',
         AUTH: 'AUTH',
         SAUTH: 'SAUTH',
         VOID: 'VOID',
         RETURN: 'RETURN',
         RETURNX: 'RETURNX'
   },
   URL: {
      CREDIT_CARD: {
         GATEWAY_CREDIT: 'service/gateway/credit',
         GATEWAY_CAPTURE: 'service/gateway/capture',
         GATEWAY_VOID: 'service/gateway/void',
         GATEWAY_INQUIRY: 'service/gateway/inquiry'
      }
   }
});