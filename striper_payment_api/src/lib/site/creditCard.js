const baseService = require('../baseService');
const appConstant = require('../appConstant');

/**
 * @description info credit card
 * @body {*} data
 * @returns token
 */
const createToken = async (stripe, data) => {
  await baseService.requiredParameters(data, ['number', 'exp_month', 'exp_year', 'cvc'])
  let result = await stripe.tokens.create({
    card: {
      number: data.number,
      exp_month: data.exp_month,
      exp_year: data.exp_year,
      cvc: data.cvc
    }
  });
  return result
}

/**
 * @description info customer
 * @body {*} data
 * @returns object customer
 */
const createCustomer = async (stripe, data) => {
  await baseService.requiredParameters(data, ['description', 'source', 'email'])
  const customer = await stripe.customers.create({
    description: data.description,
    source: data.source, // obtained with Stripe.js
    email: data.email
  });
  return customer
}

/**
 * @description info charge
 * @body {*} data
 * @returns object charge
 */
const createCharge = async (stripe, data) => {
  //await baseService.requiredParameters(data, ['amount', 'currency', 'source', 'description'])
  const charge = await stripe.charges.create({
    amount: data.amount,
    currency: data.currency,
    source: data.source, // obtained with Stripe.js
    description: data.description
  });
  return charge;
}

/**
   * @description info payment
   * @body {*} data
   * @returns object payment
   */
const  paymentMethods = async (stripe, data)=> {
    const paymentCard = await stripe.paymentMethods.create({
      type: data.type,
      card: {
        number: data.card.number,
        exp_month: data.card.exp_month,
        exp_year: data.card.exp_year,
        cvc: data.card.cvc
      }
    });
    return paymentCard
  }


/**
 * @description info product
 * @body {*} data
 * @returns object product
 */
const createProduct = async (stripe, data) => {
  await baseService.requiredParameters(data, ['name', 'type', 'description', 'attributes'])
  const product = await stripe.products.create({
    name: data.name,
    type: data.type,
    description: data.description,
    attributes: data.attributes
  });
  return product;
}

/**
 * @description info plan
 * @body {*} data
 * @returns object plan
 */
const createPlans = async (stripe, data) => {
  const plan = await stripe.plans.create({
    currency: data.currency,
    interval: data.interval,
    product: data.product,
    nickname: data.nickname,
    amount: data.amount,
  })
  return plan
}


const creatSubscribe = async (customer = 'cus_4fdAW5ftNQow1a', items = [{
  plan: 'plan_CBXbz9i7AIOTzr'
}]) => {
  const subscription = await stripe.subscriptions.create({
    customer: customer,
    items: items,
  })
  return subscription
}


module.exports = {
  createToken,
  createCustomer,
  createCharge,
  paymentMethods,
  createPlans,
  creatSubscribe,
  createProduct
}