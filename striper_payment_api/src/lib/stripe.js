const _ = require('lodash');

const appConstant = require('./appConstant');
const baseService = require('./baseService');
const creditCard = require('./site/creditCard');

const PAYMENT_TYPE = appConstant.PAYMENT_TYPE;
const PAYMENT_METHOD_CREDIT = appConstant.PAYMENT_METHOD_CREDIT;

class CreditCardApi {
    constructor(stripe) {
        this.stripe = stripe;
    }
    /**
     * @param  {*}data
     * @param  {*}method
     * @returns {object}
     */
    async creditMethod(data, method) {
        let result;
        switch (method) {
            case PAYMENT_METHOD_CREDIT.CREATE_TOKEN:
                result = await creditCard.createToken(this.stripe, data);
                break;
            case PAYMENT_METHOD_CREDIT.CREATE_CUSTOMER:
                result = await creditCard.createCustomer(this.stripe, data);
                break;
            case PAYMENT_METHOD_CREDIT.CREATE_CHARGE:
                result = await creditCard.createCharge(this.stripe, data);
                break;
            case PAYMENT_METHOD_CREDIT.CREATE_PRODUCT:
                result = await creditCard.createProduct(this.stripe, data);
                break;
            case PAYMENT_METHOD_CREDIT.CREATE_PLAN:
                result = await creditCard.createPlans(this.stripe, data);
                break;
            case PAYMENT_METHOD_CREDIT.PAYMENT_METHOD_CARD:
                result = await creditCard.paymentMethods(this.stripe, data);
                break;
            default:
                throw Error('This is not payment for stripe');
        }
        if (result) {
            return result;
            //return await baseService.convertStringToObject(result);
        }
        throw Error('支払い方法にはこの方法はありません');
    }
}

module.exports = {
    CreditCardApi,
    appConstant
}